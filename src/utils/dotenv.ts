import 'dotenv/config'

export interface IDotenv {
  ENV: 'development' | 'production' | 'test'
  IS_PRODUCTION: boolean
  PORT: number
  HOST: string
  HTTPS: boolean
  BASE_URL: string
}

export class Dotenv {
  public static load(): IDotenv {
    let port = process.env.PORT ?? 3000
    port = Number(port)

    const host = process.env.HOST ?? 'localhost'
    const https = process.env.HTTPS === 'true'
    const prefix = https ? 'https' : 'http'
    const isDev = process.env.ENV === 'development'

    let baseURL = `${prefix}://${host}`
    if (isDev)
      baseURL = `${baseURL}:${port}`

    return {
      ENV: process.env.ENV as 'development' | 'production' | 'test',
      IS_PRODUCTION: process.env.ENV === 'production',
      PORT: port,
      HOST: host,
      HTTPS: https,
      BASE_URL: baseURL,
    }
  }
}
