import type { H3Event } from 'h3'
import { handleCors } from 'h3'

export function cors(event: H3Event): void {
  handleCors(event, {
    origin: '*',
    methods: ['GET'],
    allowHeaders: ['Content-Type', 'Authorization', 'X-Requested-With', 'X-Forwarded-For'],
  })
}
