import { Dotenv } from '@/utils/dotenv'

export default async () => {
  const dotenv = Dotenv.load()
  return {
    message: 'Servant API is running!',
    endpoints: {
      fetch: {
        endpoint: `${dotenv.BASE_URL}/api/fetch`,
        example: `${dotenv.BASE_URL}/api/fetch?url=https://jsonplaceholder.typicode.com/todos/1`,
      },
      headers: {
        endpoint: `${dotenv.BASE_URL}/api/headers`,
        example: `${dotenv.BASE_URL}/api/headers`,
      },
      ip: {
        endpoint: `${dotenv.BASE_URL}/api/ip`,
        example: `${dotenv.BASE_URL}/api/ip`,
      },
      oembed: {
        endpoint: `${dotenv.BASE_URL}/api/meta`,
        example: `${dotenv.BASE_URL}/api/meta?url=https://github.com`,
      },
    },
  }
}
