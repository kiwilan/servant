import { type H3Event, createError, getQuery } from 'h3'
import { cors } from '@/utils/cors'
import { Meta } from '@/models/meta'

export default async (event: H3Event) => {
  cors(event)

  const query = getQuery(event)
  if (!query.url) {
    throw createError({
      status: 500,
      statusMessage: 'URL is required',
      data: { query },
    })
  }

  const url = query.url as string
  const meta = await Meta.make(url)

  return {
    meta: meta.getMeta(),
    title: meta.getTitle(),
    description: meta.getDescription(),
    openGraph: meta.getOpenGraph(),
    twitterCard: meta.getTwitterCard(),
  }
}
