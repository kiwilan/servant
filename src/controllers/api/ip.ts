import { type H3Event, getQuery } from 'h3'
import { Website } from '@/models/website'
import { cors } from '@/utils/cors'

export default async (event: H3Event) => {
  cors(event)

  const website = Website.make(event.headers, event)
  const query = getQuery(event)
  const format = query.format || 'json'

  if (format === 'text') {
    return website.getIp()
  }

  return {
    ip: website.getIp(),
  }
}
