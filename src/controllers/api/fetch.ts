import { type H3Event, createError, getQuery, handleCors } from 'h3'
import { FetchServer } from '@/models/fetch-server'
import { cors } from '@/utils/cors'

export default async (event: H3Event) => {
  cors(event)

  const query = getQuery(event)
  if (!query.url) {
    throw createError({
      status: 500,
      statusMessage: 'URL is required',
      data: { query },
    })
  }
  const url = query.url as string
  const fetch = await FetchServer.make(url)

  return {
    url: fetch.getUrl(),
    response: fetch.getResponse(),
    isJson: fetch.isJsonResponse(),
    error: fetch.getError(),
  }
}
