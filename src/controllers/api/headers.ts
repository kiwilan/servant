import { type H3Event, getHeaders, getQuery } from 'h3'
import { Website } from '@/models/website'
import { cors } from '@/utils/cors'

export default async (event: H3Event) => {
  cors(event)

  const url = getQuery(event).url as string | undefined
  if (url) {
    try {
      const response = await fetch(url)
      return Website.make(response.headers)
    }
    catch (error) {
      console.error(error)
    }
  }

  return Website.make(event.headers, event)
}
