import root from './root'
import api from './api'
import apiIp from './api/ip'
import apiHeaders from './api/headers'
import apiFetch from './api/fetch'
import apiMeta from './api/meta'

export {
  root,
  api,
  apiIp,
  apiHeaders,
  apiFetch,
  apiMeta,
}
