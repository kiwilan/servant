import type { H3Event } from 'h3'
import { handleCors } from 'h3'

export class Website {
  protected constructor(
    protected headers: { [key: string]: string } = {},
    protected userAgent: string = 'unknown',
    protected accept?: string,
    protected browser?: string,
    protected system?: string,
    protected ip?: string,
  ) {}

  public static make(headers: Headers, event?: H3Event): Website {
    const self = new Website()

    headers.forEach((value, key) => {
      self.headers[key] = value
    })

    self.userAgent = self.headers['user-agent'] || 'unknown'
    self.accept = self.headers.accept || undefined

    self.system = self.setSystem()
    self.browser = self.setBrowser()
    self.ip = self.setIP(event)

    return self
  }

  public getIp(): string {
    return this.ip || 'unknown'
  }

  public getSystem(): string {
    return this.system || 'unknown'
  }

  public getBrowser(): string {
    return this.browser || 'unknown'
  }

  public getHeaders(): { [key: string]: string } {
    return this.headers
  }

  public getAccept(): string {
    return this.accept || 'unknown'
  }

  public getUserAgent(): string {
    return this.userAgent
  }

  private setIP(event?: H3Event): string {
    const cfConnectingIp = this.headers['cf-connecting-ip'] as string // Cloudflare
    const cfTrueClientIp = this.headers['cf-true-client-ip'] as string // Cloudflare
    const xRealIp = this.headers['x-real-ip'] as string // Nginx
    const xForwardedFor = this.headers['x-forwarded-for'] as string // Nginx
    const remoteAddress = event?.node.req.socket.remoteAddress

    return cfConnectingIp
      ?? cfTrueClientIp
      ?? xRealIp
      ?? xForwardedFor
      ?? remoteAddress
      ?? 'unknown'
  }

  private setSystem(): string {
    let system = 'unknown'

    const systems: { [key: string]: RegExp } = {
      windows: /windows/i,
      macos: /macintosh/i,
      ios: /ipad|iphone/i,
      android: /android/i,
      linux: /linux/i,
    }

    for (const key in systems) {
      if (systems[key].test(this.userAgent)) {
        system = key
        break
      }
    }

    return system
  }

  private setBrowser(): string {
    let browser = 'unknown'

    const browsers: { [key: string]: RegExp } = {
      chrome: /chrome/i,
      safari: /safari/i,
      firefox: /firefox/i,
      ie: /internet explorer/i,
      brave: /brave/i,
      opera: /opera/i,
      edge: /edg/i,
    }

    for (const key in browsers) {
      if (browsers[key].test(this.userAgent)) {
        browser = key
        break
      }
    }

    return browser
  }
}
