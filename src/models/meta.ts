import { JSDOM } from 'jsdom'
import { FetchServer } from './fetch-server'

interface OpenGraph {
  title?: string | null
  description?: string | null
  image?: string | null
  imageAlt?: string | null
  imageType?: string | null
  imageWidth?: string | null
  imageHeight?: string | null
  url?: string | null
  site_name?: string | null
  type?: string | null
}

interface TwitterCard {
  card?: string | null
  title?: string | null
  description?: string | null
  image?: string | null
  site?: string | null
  creator?: string | null
}

export class Meta {
  protected constructor(
    protected url: string,
    protected meta: { [key: string]: string | null } = {},
    protected title: string | null = null,
    protected description: string | null = null,
    protected openGraph: OpenGraph = {},
    protected twitterCard: TwitterCard = {},
  ) {}

  public static async make(url: string): Promise<Meta> {
    const self = new this(url)

    const fetch = await FetchServer.make(url)

    const htmlBody = fetch.getResponse().body
    const dom = new JSDOM(htmlBody)
    const document = dom.window.document

    const meta = document.querySelectorAll('meta')
    meta.forEach((meta) => {
      const name = meta.getAttribute('name')
      const property = meta.getAttribute('property')
      const content = meta.getAttribute('content')

      if (property) {
        self.meta[property] = content
      }
      else if (name && content) {
        self.meta[name] = content
      }
    })

    self.parseOpenGraph()
    self.parseTwitterCard()

    self.title = document.title
    self.description = self.meta.description || self.meta['og:description'] || self.meta['twitter:description'] || null

    return self
  }

  public getMeta(): { [key: string]: string | null } {
    return this.meta
  }

  public getTitle(): string | null {
    return this.title
  }

  public getDescription(): string | null {
    return this.description
  }

  public getOpenGraph(): OpenGraph {
    return this.openGraph
  }

  public getTwitterCard(): TwitterCard {
    return this.twitterCard
  }

  private parseOpenGraph(): void {
    this.openGraph.title = this.meta['og:title'] || null
    this.openGraph.description = this.meta['og:description'] || null
    this.openGraph.image = this.meta['og:image'] || null
    this.openGraph.imageAlt = this.meta['og:image:alt'] || null
    this.openGraph.imageType = this.meta['og:image:type'] || null
    this.openGraph.imageWidth = this.meta['og:image:width'] || null
    this.openGraph.imageHeight = this.meta['og:image:height'] || null
    this.openGraph.url = this.meta['og:url'] || null
    this.openGraph.site_name = this.meta['og:site_name'] || null
    this.openGraph.type = this.meta['og:type'] || null
  }

  private parseTwitterCard(): void {
    this.twitterCard.card = this.meta['twitter:card'] || null
    this.twitterCard.title = this.meta['twitter:title'] || null
    this.twitterCard.description = this.meta['twitter:description'] || null
    this.twitterCard.image = this.meta['twitter:image:src'] || null
    this.twitterCard.site = this.meta['twitter:site'] || null
    this.twitterCard.creator = this.meta['twitter:creator'] || null
  }
}
