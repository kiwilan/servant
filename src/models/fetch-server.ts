import { Website } from './website'

interface FetchServerResponse {
  ok: boolean
  status: number
  statusText: string
  url?: string
  time: number
  meta?: {
    domain?: string
    useHttps?: boolean
    isRedirected?: boolean
    icon?: string
  }
  headers?: { [key: string]: string | null }
  body?: string
}

export class FetchServer {
  protected constructor(
    protected url: string,
    protected response: FetchServerResponse,
    protected isJson: boolean = false,
    protected error?: string,
  ) {}

  public static async make(url: string): Promise<FetchServer> {
    const response: FetchServerResponse = {
      ok: false,
      status: 500,
      statusText: 'Unknown error',
      time: 0,
    }

    const self = new this(url, response)
    const res = await self.fetchUrl()

    if (!res)
      return self

    await self.parseResponse(res)

    return self
  }

  public getUrl(): string {
    return this.url
  }

  public getResponse(): FetchServerResponse {
    return this.response
  }

  public isJsonResponse(): boolean {
    return this.isJson
  }

  public getError(): string | undefined {
    return this.error
  }

  private async parseResponse(res: Response): Promise<void> {
    const contentType = res.headers.get('content-type')
    this.isJson = contentType?.includes('application/json') ?? false
    const body = this.isJson
      ? await res.json()
      : await res.text()

    const website = Website.make(res.headers)

    this.response = {
      ok: res.ok,
      url: res.url,
      status: res.status,
      statusText: res.statusText,
      time: this.response.time,
      meta: {
        domain: (new URL(res.url)).hostname,
        useHttps: res.url.startsWith('https'),
        isRedirected: res.redirected,
        icon: this.findIcon(body),
      },
      headers: website.getHeaders(),
      body,
    }
  }

  private findIcon(body: string): string | undefined {
    if (this.isJson)
      return undefined

    const links = body.match(/<link[^>]*>/gi)
    if (!links)
      return undefined

    let icon: string | undefined
    let match: RegExpExecArray | null

    const regex = /<link[^>]*rel="shortcut icon"[^>]*href="([^"]*)"[^>]*>/i
    match = regex.exec(body)

    if (!match) {
      const regex = /<link[^>]*rel="apple-touch-icon"[^>]*href="([^"]*)"[^>]*>/i
      match = regex.exec(body)
    }

    if (!match) {
      const regex = /<link[^>]*rel="icon"[^>]*href="([^"]*)"[^>]*>/i
      match = regex.exec(body)
    }

    if (match && match[1]) {
      const iconMatch = match[1]
      if (iconMatch.startsWith('http')) {
        icon = iconMatch
      }
      else {
        const url = new URL(iconMatch, this.url)
        icon = url.href
      }
    }
    else {
      const url = new URL('/favicon.ico', this.url)
      icon = url.href
    }

    return icon
  }

  private async fetchUrl(): Promise<Response | false> {
    let response: Response
    const start = Date.now()
    try {
      response = await fetch(this.url)
      const end = Date.now()
      this.response.time = end - start
    }
    catch (error) {
      this.error = JSON.stringify(error)
      this.response.statusText = this.error
      return false
    }

    return response
  }
}
