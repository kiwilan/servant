import { cwd } from 'node:process'
import { resolve } from 'node:path'
import { FsFile } from '@kiwilan/filesystem'
import playwright from 'playwright'

export class BrowserKit {
  protected constructor(
    protected url: string,
    protected downloadPath?: string,
    protected browser?: playwright.Browser,
    protected page?: playwright.Page,
    protected context?: playwright.BrowserContext,
    protected timeout = 50000,
  ) {}

  public static async make(url: string): Promise<BrowserKit> {
    const self = new this(url)

    const path = `${cwd()}/src/public/downloads`
    if (!await FsFile.exists(path))
      await FsFile.makeDirectory(path, true)
    self.downloadPath = await self.prepareDownload()
    await self.prepareBrowser()

    return self
  }

  public async googleDriveDownload(): Promise<string | undefined> {
    if (!this.page || !this.browser || !this.context)
      throw new Error('Browser not prepared')

    await this.page.goto(this.url)

    const selector = 'body > div.ndfHFb-c4YZDc.ndfHFb-c4YZDc-AHmuwe-Hr88gd-OWB6Me.dif24c.vhoiae.LgGVmb.bvmRsc.ndfHFb-c4YZDc-vyDMJf-aZ2wEe.ndfHFb-c4YZDc-i5oIFb.ndfHFb-c4YZDc-uoC0bf.ndfHFb-c4YZDc-TSZdd > div.ndfHFb-c4YZDc-Wrql6b > div > div.ndfHFb-c4YZDc-Wrql6b-AeOLfc-b0t70b > div.ndfHFb-c4YZDc-Wrql6b-LQLjdd > div.ndfHFb-c4YZDc-Wrql6b-C7uZwb-b0t70b > div:nth-child(3)'
    await this.page.waitForSelector(selector)
    await this.page.click(selector)

    await this.page.waitForTimeout(1000)

    let downloadPath = `${this.downloadPath}/`
    if (this.context.pages().length > 1) {
      const lastPage = this.context.pages()[this.context.pages().length - 1]
      const downloaded = lastPage.waitForEvent('download', { timeout: this.timeout })

      await lastPage.waitForSelector('#uc-download-link')
      await lastPage.click('#uc-download-link')

      const download = await downloaded
      downloadPath += download.suggestedFilename()
      await download.saveAs(downloadPath)
    }
    else {
      const downloaded = this.page.waitForEvent('download', { timeout: this.timeout })
      const download = await downloaded
      downloadPath += download.suggestedFilename()
      await download.saveAs(downloadPath)
    }

    await this.browser.close()

    return downloadPath
  }

  private async prepareBrowser(): Promise<void> {
    this.browser = await playwright.firefox.launch({
      headless: true,
    })
    this.context = await this.browser.newContext()
    this.page = await this.context.newPage()
  }

  private async prepareDownload(): Promise<string> {
    const downloadPath = resolve(cwd(), './src/public/downloads')
    await FsFile.cleanDirectory(downloadPath)

    const key = (Math.random() + 1).toString(36).substring(2)
    const downloadKeyPath = `${downloadPath}/${key}`

    await FsFile.makeDirectory(downloadKeyPath, true)

    return downloadKeyPath
  }
}
