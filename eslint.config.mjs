import antfu from '@antfu/eslint-config'

export default antfu({
  ignores: [
    //
  ],
}, {
  rules: {
    'no-console': 'warn',
    'node/prefer-global/process': 'off',
  },
})
