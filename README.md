# Servant

[![version][version-src]][version-href]
[![h3][h3-version-src]][h3-version-href]
[![node][node-version-src]][node-version-href]
[![License][license-src]][license-href]

Useful tools for developers.

## Docker

Create `.env` file:

```bash
cp .env.example .env
```

- `ENV`: environment can be `development`, `production`, `test`
- `PORT`: port of the app, default is `3000`
- `HOST`: host of the app, default is `localhost`
- `HTTPS`: `true` or `false`
- `APP_PORT`: port for Docker, default is `3000`

Run with Docker Compose:

```bash
docker compose down -v
docker compose up --build -d --remove-orphans
```

You can deploy with [nginx.conf](./docker/example.conf) for NGINX.

## Usage

- `/api/fetch`: fetch data from URL (query parameter `url` is required)
- `/api/headers`: get headers from request
- `/api/ip`: get IP address from request (query parameter `format=text` for text format)
- `/api/meta`: parse meta tags from URL (query parameter `url` is required)

## License

[MIT](LICENSE)

[version-src]: https://img.shields.io/badge/dynamic/json?label=version&query=version&url=https://gitlab.com/kiwilan/servant/-/raw/main/package.json&colorA=18181B&colorB=F0DB4F
[version-href]: https://gitlab.com/kiwilan/servant/-/tags
[h3-version-src]: https://img.shields.io/badge/dynamic/json?label=h3&query=dependencies['h3']&url=https://gitlab.com/kiwilan/servant/-/raw/main/package.json&colorA=18181B&colorB=F0DB4F
[h3-version-href]: https://github.com/unjs/h3
[license-src]: https://img.shields.io/badge/licence-MIT-blue?style=flat-square&colorA=18181B&colorB=F0DB4F
[license-href]: https://gitlab.com/kiwilan/servant/blob/main/README.md
[node-version-src]: https://img.shields.io/badge/dynamic/json?label=Node.js&query=engines[%27node%27]&url=https://gitlab.com/kiwilan/servant/-/raw/main/package.json&style=flat-square&colorA=18181B&colorB=F0DB4F
[node-version-href]: https://nodejs.org/en/
